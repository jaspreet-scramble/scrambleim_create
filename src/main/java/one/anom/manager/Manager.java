package one.anom.manager;

import one.anom.manager.controller.UserController;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

import static spark.Spark.get;
import static spark.Spark.ipAddress;
import static spark.Spark.post;
import static spark.Spark.port;

public class Manager {

    public static String DOMAIN = "said.scrambleim.com";
    public static String SERVICE = "scrambleim.com";
    public static void main(String... args) {
        System.out.println("Hello world");
        ipAddress("said.scrambleim.com");
        port(443);
        final TemplateEngine templateEngine = new FreeMarkerEngine();
        get("/", UserController.addUserScreen, templateEngine);
        post("/", UserController.createUser);
        post("/delete/", UserController.delteUser);
        post("/reset/", UserController.resetUser);
    }

}

















