package one.anom.manager.controller;

import de.gultsch.ejabberd.api.RequestFailedException;
import de.gultsch.ejabberd.api.hla.Ejabberd;
import one.anom.manager.Manager;
import one.anom.manager.utils.CryptoHelper;
import spark.ModelAndView;
import spark.Route;
import spark.TemplateViewRoute;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class UserController {

    private static final String SALT = "G5BwOUZ5Kx";


    public static TemplateViewRoute addUserScreen = (request, response) -> {
        HashMap<String,Object> model = new HashMap<>();
        String imei = URLDecoder.decode(request.queryParamOrDefault("imei",""),"UTF-8");
        Status status;
        String statusParam = request.queryParams("status");
        if (statusParam != null) {
            try {
                status = Status.valueOf(statusParam);
            } catch (IllegalArgumentException e) {
                status = Status.NONE;
            }
        } else {
            status = Status.NONE;
        }
        model.put("imei",imei);
        switch (status) {
            case USER_EXISTS:
                model.put("message","This user already exists");
                break;
            case USER_DOES_NOT_EXISTS:
                model.put("message","This user does't exists");
                break;
            case USER_RESET:
                model.put("message","This user has been reset");
                break;
            case USER_CREATED:
                model.put("message","This user has been created");
                break;
            case INVALID_IMEI:
                model.put("message","The IMEI you entered is not valid");
                break;
                
            case USER_DELETED:
                model.put("message","The user has been deleted");
                break;
                
            case FAILED:
                model.put("message","Something went wrong");
                break;
            default:
                break;
        }

        return new ModelAndView(model,"add_user.ftl");
    };

    public static Route createUser = (request, response) -> {
        String imei = request.queryParams("imei");
        if (imei == null || !imei.matches("[0-9]+") || imei.length() != 15) {
            response.redirect(buildUrl(Status.INVALID_IMEI,imei));
            return null;
        }
        //EjabberdApi api = new EjabberdApi("https://xmpp.anom.one/api/", "admin", "test1234");
        Ejabberd ejabberd = new Ejabberd();
        //api.setIgnoreSllExceptions(true);
        String password = toPassword(imei);
        String username = password.substring(password.length() - 6);
        System.out.println("username: "+username+" password:"+password);
        try {
            ejabberd.register(username,Manager.SERVICE,password);
            response.redirect(buildUrl(Status.USER_CREATED));
        } catch (RequestFailedException e) {
            if (e.getCode() == 10090) {
                response.redirect(buildUrl(Status.USER_EXISTS, imei));
            } else {
                System.err.println(e.getMessage());
                response.redirect(buildUrl(Status.FAILED, imei));
            }
        }
        return null;
    };
    
    
    public static Route delteUser = (request, response) -> {
        String imei = request.queryParams("imei");
        if (imei == null || !imei.matches("[0-9]+") || imei.length() != 15) {
            response.redirect(buildUrl(Status.INVALID_IMEI,imei));
            return null;
        }
        //EjabberdApi api = new EjabberdApi("https://xmpp.anom.one/api/", "admin", "test1234");
        Ejabberd ejabberd = new Ejabberd();
        //api.setIgnoreSllExceptions(true);
        String password = toPassword(imei);
        String username = password.substring(password.length() - 6);
        System.out.println("username: "+username+" password:"+password);
        try {
        	if(ejabberd.checkAccount(username, Manager.SERVICE)) {
	            ejabberd.unregister(username,Manager.SERVICE);
	            response.redirect(buildUrl(Status.USER_DELETED));
        	}else {
        		 response.redirect(buildUrl(Status.USER_DOES_NOT_EXISTS));
        	}
        } catch (RequestFailedException e) {
                System.err.println(e.getMessage());
                response.redirect(buildUrl(Status.FAILED, imei));
        }
        return null;
    };
    
    public static Route resetUser = (request, response) -> {
        String imei = request.queryParams("imei");
        if (imei == null || !imei.matches("[0-9]+") || imei.length() != 15) {
            response.redirect(buildUrl(Status.INVALID_IMEI,imei));
            return null;
        }
        //EjabberdApi api = new EjabberdApi("https://xmpp.anom.one/api/", "admin", "test1234");
        Ejabberd ejabberd = new Ejabberd();
        //api.setIgnoreSllExceptions(true);
        String password = toPassword(imei);
        String username = password.substring(password.length() - 6);
        System.out.println("username: "+username+" password:"+password);
        try {
        	if(ejabberd.checkAccount(username, Manager.SERVICE)) {
	        	ejabberd.changePassword(username,Manager.SERVICE,password);
	            response.redirect(buildUrl(Status.USER_RESET));
        	}else {
       		 response.redirect(buildUrl(Status.USER_DOES_NOT_EXISTS));
       	}
        } catch (RequestFailedException e) {
                System.err.println(e.getMessage());
                response.redirect(buildUrl(Status.FAILED, imei));
        }
        return null;
    };

    private static String toPassword(String imei) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final String combined = imei+SALT;
            messageDigest.update(combined.getBytes("UTF-8"));
            byte[] bytes = messageDigest.digest();
            return CryptoHelper.bytesToHex(bytes);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new AssertionError();
        }

    }

    
    public static void main(String[] args) {
    	String password = toPassword("987654321012345");
    	System.out.println("password = "+ password);
		System.out.println("username = "+ password.substring(password.length() - 6));
	}
    
    private static String buildUrl(Status status) {
        return buildUrl(status, null);
    }

    private static String buildUrl(Status status, String imei) {
        StringBuilder url = new StringBuilder();
        url.append("https://").append(Manager.DOMAIN).append("/?status=").append(status.toString());
        if (imei != null && !imei.isEmpty()) {
            try {
                url.append("&imei=").append(URLEncoder.encode(imei,"UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError();
            }
        }
        return url.toString();
    }

    public enum Status {
        USER_EXISTS,
        USER_DOES_NOT_EXISTS,
        USER_CREATED,
        USER_RESET,
        USER_DELETED,
        INVALID_IMEI,
        FAILED,
        NONE
    }
}
