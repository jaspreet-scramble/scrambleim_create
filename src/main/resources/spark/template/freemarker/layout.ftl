<#macro layout title="">
<!DOCTYPE html>

<html lang="en">
<head>
  <title>${title} - Anom.one</title>
</head>
<body>
  <#nested/>
</body>
</html>
</#macro>