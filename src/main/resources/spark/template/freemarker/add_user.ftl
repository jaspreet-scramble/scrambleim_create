<#ftl output_format="HTML">
<#import "layout.ftl" as layout />
<@layout.layout title="Add user">
<#if message??>
  <p>${message}</p>
</#if>
<form method="post" action="/">
  <div class="form-group">
    <label for="imei">IMEI to register user</label>
    <input type="text" name="imei" required value="${imei}""/>
  </div>
  <button type="submit">Create user</button>
</form>

<form method="post" action="/delete/">
  <div class="form-group">
    <label for="imei">IMEI to delete user</label>
    <input type="text" name="imei" required value="${imei}""/>
  </div>
  <button type="submit">Delete user</button>
</form>

<form method="post" action="/reset/">
  <div class="form-group">
    <label for="imei">IMEI to reset user</label>
    <input type="text" name="imei" required value="${imei}""/>
  </div>
  <button type="submit">Reset user</button>
</form>

</@layout.layout>